﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using visaRestApi.Entities;
using visaRestApi.Models.AccountManagemt;

namespace visaRestApi.Models.Validators
{
    public class RegsiterUserDtoValidator : AbstractValidator<RegsiterUserDto>
    {
        public RegsiterUserDtoValidator(visaRestApiDbContext dbContext)
        {
            RuleFor(n => n.Email).NotEmpty().EmailAddress();

            RuleFor(n => n.Password).MinimumLength(6);

            RuleFor(n => n.ConfrimPassword).Equal(s => s.Password);

            RuleFor(n => n.Email).Custom((value, context) =>
                {
                    var inUse = dbContext.Users.Any(x => x.Email == value);
                    if (inUse)
                    {
                        context.AddFailure("email", "That email is taken");
                    }
                });
            RuleFor(n => n.UserName).Custom((value, context) =>
            {
                var inUse = dbContext.Users.Any(x => x.UserName == value);
                if (inUse)
                {
                    context.AddFailure("User Name", "Is alredy taken");
                }
            });
        }
    }
}
