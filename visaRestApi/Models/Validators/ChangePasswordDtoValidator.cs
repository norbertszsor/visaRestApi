﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using visaRestApi.Entities;
using visaRestApi.Models.AccountManagemt;

namespace visaRestApi.Models.Validators
{
    public class ChangePasswordDtoValidator: AbstractValidator<ChangePasswordDto>
    {
        public ChangePasswordDtoValidator(visaRestApiDbContext dbContext)
        {


            RuleFor(n => n.Password).MinimumLength(6);

            RuleFor(n => n.ConfrimPassword).Equal(s => s.Password);

          
        }
    }
}
