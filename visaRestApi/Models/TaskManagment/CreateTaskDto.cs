﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace visaRestApi.Models.TaskManagment
{
    public class CreateTaskDto
    {
        public string Title { get; set; }
        public string ManagerNotes { get; set; }
    }
}
