﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace visaRestApi.Models.TaskManagment
{
    public class UsersAllTasksDto
    {
        public string Title { get; set; }
        public string CreateDate { get; set; }
        public string ExpireDate { get; set; }
        public string FinishData { get; set; }
        public string UserNotes { get; set; }
        public string ManagerNotes { get; set; }
        public bool UserFinishTask { get; set; }
        public bool MenagerAccept { get; set; }
    }
}
