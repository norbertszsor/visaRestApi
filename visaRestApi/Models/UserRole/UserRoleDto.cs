﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace visaRestApi.Models
{
    public class UserRoleDto
    {
        public string RoleName { get; set; }
    }
}
