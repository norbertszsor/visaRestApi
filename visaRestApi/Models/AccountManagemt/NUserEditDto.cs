﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace visaRestApi.Models.AccountManagemt
{
    public class NUserEditDto
    {
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Phone { get; set; }
    }
}
