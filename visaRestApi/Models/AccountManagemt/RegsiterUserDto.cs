﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace visaRestApi.Models.AccountManagemt
{
    public class RegsiterUserDto
    {
      
        public string Email { get; set; }
        
        public string Password{ get; set; }
        public string ConfrimPassword { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        [MaxLength(9)]
        public string Phone { get; set; }
        public int RoleId { get; set; } = 3;
        public int GroupNumber { get; set; } = 0;


    }
}
