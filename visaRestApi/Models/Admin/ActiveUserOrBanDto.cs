﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace visaRestApi.Models.Admin
{
    public class ActiveUserOrBanDto
    {
        public bool IsActivated { get; set; }
        public bool IsBanned { get; set; }
    }
}
