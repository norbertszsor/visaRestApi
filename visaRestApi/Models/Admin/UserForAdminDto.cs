﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using visaRestApi.Entities;

namespace visaRestApi.Models.Admin
{
    public class UserForAdminDto
    {
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Phone { get; set; }
        public string RoleName { get; set; }
        public int GroupNumber { get; set; }
        public bool IsActivated { get; set; }
        public bool IsBanned { get; set; }
    }
}
