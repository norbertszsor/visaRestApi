﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace visaRestApi.Entities
{
    public class Tasks 
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string CreateDate { get; set; }
        public string ExpireDate { get; set; }
        public string FinishData { get; set; }
        public string UserNotes { get; set; }
        public string ManagerNotes { get; set; }
        public bool UserFinishTask { get; set; }
        public bool MenagerAccept { get; set; }

        [ForeignKey("SlaveUser")]
        public virtual User UserSlave { get; set; }
        [ForeignKey("MasterUser")]
        public virtual User UserMaster { get; set; }


    }
}
