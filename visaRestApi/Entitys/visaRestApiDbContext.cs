﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace visaRestApi.Entities
{
    public class visaRestApiDbContext : DbContext
    {
        private string _connectionString = "Server=(localdb)\\MSSQLLocalDB;Initial Catalog=visaRestDB;Integrated Security=True;Connect Timeout=30;" +
            "Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        public DbSet<User> Users { get; set; }
        public DbSet<Role> UsersRoles{ get; set; }
        public DbSet<Tasks> UsersTasks { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {           
            optionsBuilder.UseSqlServer(_connectionString);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().Property(n => n.Email).IsRequired().HasMaxLength(25);
            modelBuilder.Entity<Role>().Property(n => n.Name).IsRequired().HasMaxLength(15);
        }
    }
}
    