﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace visaRestApi.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public string UserName { get; set; }       
        public string Phone { get; set; } 
        public int RoleId { get; set; }
        public int GroupNumber { get; set; }
        public bool IsActivated { get; set; }
        public bool IsBanned { get; set; }
        public virtual Role Role { get; set; }
    }
}
