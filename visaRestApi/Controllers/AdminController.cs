﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using visaRestApi.Models.AccountManagemt;
using visaRestApi.Models.Admin;
using visaRestApi.Services;

namespace visaRestApi.Controllers
{
    [ApiController]
    [Route("api/admin")]
    [Authorize(Roles = "admin")]
    public class AdminController : ControllerBase
    {
        private readonly IAccountService _accountService;
        private readonly IUsersService _userServices;

        public AdminController(IAccountService accountService, IUsersService userService)
        {
            _accountService = accountService;
            _userServices = userService;
        }
      
        [HttpGet("user/getAll")]
        public ActionResult GetAllUsers()
        {
            var state = _userServices.GetAllUsers();

            return Ok(state);
        }

        [HttpGet("user/getUser/{id}")]
        public ActionResult GetUserById([FromRoute] int id)
        {
            var state = _userServices.GetUser(id);
            if (state == null)
            {
                return NoContent();
            }

            return Ok(state);
        }
        [HttpPost("user/createUser")]
        public ActionResult CreateUser([FromBody] RegsiterUserDto newUser)
        {
            var state = _userServices.CreateUser(newUser);

            if (state == false) return BadRequest();

            return Ok(state);

        }
        [HttpPut("user/editUser/{id}")]
        public ActionResult EditUser([FromRoute] int id, [FromBody] UserEditForAdminDto userToEdit)
        {
            var state = _userServices.EditUser(id, userToEdit);
            if (state == false) return BadRequest();
            return Ok(state);
        }

        [HttpPut("user/ban/{id}")]
        public ActionResult BanUser([FromRoute] int id, [FromBody] ActiveUserOrBanDto dto)
        {
            var state = _accountService.BanUser(id, dto);

            if (state == false)
            {
                return BadRequest();
            }
            return Ok(state);
        }
        [HttpPut("user/active/{id}")]
        public ActionResult ActiveUser([FromRoute] int id, [FromBody] ActiveUserOrBanDto dto)
        {
            var state = _accountService.ActiveUser(id, dto);

            if (state == false)
            {
                return BadRequest();
            }
            return Ok(state);
        }
        [HttpDelete("user/deleteUser/{id}")]
        public ActionResult DeleteUser([FromRoute] int id)
        {
            var state = _userServices.DeleteUser(id);
            if (state == false) return NoContent();
            return Ok(state);
        }

     

    }
}
