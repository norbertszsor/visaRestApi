﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using visaRestApi.Models.TaskManagment;
using visaRestApi.Services;

namespace visaRestApi.Controllers
{
    [ApiController]
    [Route("api/worker")]
    [Authorize(Roles = "worker")]
    public class UserTaskController : ControllerBase
    {
        private readonly IAccountService _accountService;
        private readonly IUsersService _userServices;
        private readonly ITasksService _tasksService;

        public UserTaskController(IAccountService accountService, IUsersService userServices, ITasksService tasksService)
        {
            _accountService = accountService;
            _userServices = userServices;
            _tasksService = tasksService;
        }

        [HttpGet("getAll")]
        public ActionResult GetAllUserTask()
        {
            var state = _tasksService.getYourTask();
            if (state == null) return BadRequest();
            return Ok(state);
        }
        [HttpPut("editTask/{id}")]
        public ActionResult EditUserTask([FromRoute] int id, [FromBody] string newTask)
        {
            var state = _tasksService.EditUserTask(id, newTask);
            if (state == false) return BadRequest();
            return Ok(state);
        }
        [HttpPut("finshTask/{id}")]
        public ActionResult FinishUserTask([FromRoute] int id)
        {
            var state = _tasksService.finnalizeTask(id);
            if (state == false) return BadRequest();
            return Ok(state);
        }
        
    }
}
