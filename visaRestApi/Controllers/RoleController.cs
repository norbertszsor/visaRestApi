﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using visaRestApi.Entities;
using visaRestApi.Models;
using visaRestApi.Services;

namespace visaRestApi.Controllers
{
    [ApiController]
    [Route("api/role")]
    [Authorize(Roles = "admin")]
    public class RoleController : ControllerBase
    {
      
        private readonly IRoleService _roleService;

        public RoleController(visaRestApiDbContext dbContext, IMapper mapper, IRoleService roleservice)
        {
            _roleService = roleservice;
        }
        [HttpGet("getAll")]   
        public ActionResult<IEnumerable<Role>> GetAll()
        {
            var roles = _roleService.GetAll();           
            return StatusCode(200, roles);
        }
        [HttpGet("getRole/{id}")]
        public ActionResult GetOneRole([FromRoute] int id)
        {
            var role = _roleService.GetOne(id);
            if (role == null) return NoContent();
            return Ok(role);
        }
        [HttpPost("add")]
        public ActionResult CreateRole([FromBody] CreateRoleDto dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var roleId = _roleService.Create(dto);

            return Created($"/api/role/{roleId}",null);
        }   
        [HttpPut("edit/{id}")]
        public ActionResult Edit([FromRoute] int id,[FromBody] UserRoleDto dto)
        {
            var state = _roleService.Edit(id, dto.RoleName);
            if (state== false)
            {
                return NotFound();
            }

            return Ok();
        }
        [HttpDelete("delete/{id}")]
        public ActionResult Delete([FromRoute] int id)
        {
            var state = _roleService.Delete(id);

            if (state)
            {
                return NoContent();
            }

            return NotFound();
        }
    }
}
