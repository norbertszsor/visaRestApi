﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using visaRestApi.Models.TaskManagment;
using visaRestApi.Services;

namespace visaRestApi.Controllers
{
    [ApiController]
    [Route("/api/manager")]
    [Authorize(Roles = "manager")]
    public class ManagerTaskController : ControllerBase
    {
        private readonly IAccountService _accountService;
        private readonly IUsersService _userServices;
        private readonly ITasksService _tasksService;

        public ManagerTaskController(IAccountService accountService, IUsersService userServices, ITasksService tasksService)
        {
            _accountService = accountService;
            _userServices = userServices;
            _tasksService = tasksService;
        }

        [HttpGet("yourWorkers")]
        public ActionResult GetUsers()
        {
            var state = _tasksService.GetYourUsers();
            if (state == null) return NoContent();
            return Ok(state);
        }
        [HttpGet("workerTasks/{id}")]
        public ActionResult GetUserTasks([FromRoute] int id)
        {
            var state = _tasksService.GetAllTasks(id);

            return Ok(state);
        }

        [HttpPost("createTask")]
        public ActionResult CreateTask([FromBody] CreateTaskDto newTask,[FromHeader] int slaveId)
        {
            _tasksService.CreateTask(newTask, slaveId);

            return Ok();
        }

        [HttpPut("editTask/{id}")]
        public ActionResult EditTask([FromBody] ManagerEditTaskDto newTask,[FromRoute] int id)
        {
            var state = _tasksService.EditTask(newTask, id);
            if (state == false) return BadRequest();
            return Ok(state);
        }
        [HttpPut("acceptTakFinalize/{id}")]
        public ActionResult finishTask([FromRoute] int id)
        {
            var state = _tasksService.AcceptUserTaskEnd(id);
            if (state == false) return BadRequest();
            return Ok(state);
        }
        [HttpDelete("DeleteTask/{id}")]
        public ActionResult DeleteTask([FromRoute] int id)
        {
            var state = _tasksService.DeleteTask(id);
            if (state == false) return BadRequest();
            return Ok(state);
        }

       
    }
}
