﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using visaRestApi.Models.AccountManagemt;
using visaRestApi.Services;

namespace visaRestApi.Controllers
{
    [Route("api/account")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IAccountService _accountService;
        private readonly IUsersService _userServices;

        public AccountController(IAccountService accountService, IUsersService userServices)
        {
            _accountService = accountService;
            _userServices = userServices;
        }

        [HttpPost("register")]
        public ActionResult RegisterUser([FromBody]RegsiterUserDto dto)
        {
            _accountService.RegisterUser(dto);

            return Ok();
        }
        [HttpPost("login")]
        public ActionResult Login([FromBody]LoginUserDto dto)
        {
            string token = _accountService.GenerateToken(dto);
            return Ok(token);
        }
        [HttpGet("detalis")]
        [Authorize]
        public ActionResult GetAccountDetalis()
        {
            var state = _userServices.GetDeatails();
            if (state.AUser == null)
            {
                return Ok(state.NUser);

            }else if(state.NUser == null)
            {
                return Ok(state.AUser);

            }else if(state == (null, null))
            {
                return BadRequest();
            }
            return Ok(state);
        }
        [HttpPut("editProfile")]
        [Authorize]
        public ActionResult EditProfile([FromBody] NUserEditDto userIfno)
        {
            var state = _userServices.EditProfile(userIfno);
            if (state == false) return BadRequest("This User cannot edit his profile");
            return Ok(true);
        }

        [HttpPut("changePassword")]
        [Authorize]
        public ActionResult ChangePassword([FromBody] ChangePasswordDto newPassword)
        {
            _userServices.ChangePassword(newPassword);
            return Ok();
        }

    }
}
