﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using visaRestApi.Models;
using visaRestApi.Entities;
using visaRestApi.Models.Admin;
using visaRestApi.Models.AccountManagemt;
using visaRestApi.Models.TaskManagment;

namespace visaRestApi.Mapping
{
    public class ProfileMapping : Profile
    {
        public ProfileMapping()
        {
            CreateMap<CreateRoleDto, Role>().ReverseMap();
            CreateMap<Role, UserRoleDto>();

            CreateMap<User, UserForAdminDto>().ReverseMap().ForMember(o=>o.Role,n=>n.MapFrom(x=>x.RoleName));
            CreateMap<UserForAdminDto, User>().ReverseMap().ForMember(o => o.RoleName, n => n.MapFrom(x => x.RoleId));


            CreateMap<User, SlaveUserDto>().ReverseMap();

            CreateMap<User, UserDetailsDto>().ReverseMap().ForMember(o => o.Role, n => n.MapFrom(x => x.RoleName));
            CreateMap<User, UserAdminDetailsDto>().ReverseMap().ForMember(o => o.Role, n => n.MapFrom(x => x.RoleName));

            CreateMap<Tasks, UsersAllTasksDto>().ReverseMap();
            CreateMap<Task, UserTaskDto>().ReverseMap();

            
        }
    }
}
