﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using visaRestApi.Entities;
using visaRestApi.Models.AccountManagemt;
using visaRestApi.Models.Admin;

namespace visaRestApi.Services
{
    public interface IUsersService
    {
        List<UserForAdminDto> GetAllUsers();
        UserForAdminDto GetUser(int id);
        bool CreateUser(RegsiterUserDto newUser);
        bool DeleteUser(int id);
        bool EditUser(int id, UserEditForAdminDto userToEdit);
        (UserDetailsDto NUser, UserAdminDetailsDto AUser) GetDeatails();
        bool EditProfile(NUserEditDto userEdit);
        void ChangePassword(ChangePasswordDto newPassword);
    }

    public class UserServices : IUsersService
    {
        private readonly visaRestApiDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly IUserContextService _userContext;

        public UserServices(visaRestApiDbContext dbContext, IMapper mapper, IPasswordHasher<User> passwordHasher, IUserContextService userContext)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _passwordHasher = passwordHasher;
            _userContext = userContext;
        }

        public List<UserForAdminDto> GetAllUsers()
        {
            var users = _dbContext.Users.Include(x=>x.Role).ToList();
            var map = _mapper.Map<List<UserForAdminDto>>(users);

            return map;
        }

        public UserForAdminDto GetUser(int id)
        {
            var user = _dbContext.Users.Include(n=>n.Role).FirstOrDefault(x => x.Id == id);
            if (user == null) return null;
            return _mapper.Map<UserForAdminDto>(user);
        }

        public bool CreateUser(RegsiterUserDto newUser)
        {
            var User = new User()
            {
                Email = newUser.Email,
                UserName = newUser.UserName,
                Phone = newUser.Phone,
                RoleId = newUser.RoleId,
                GroupNumber = newUser.GroupNumber
            };
            var HPassword = _passwordHasher.HashPassword(User, newUser.Password);
            User.PasswordHash = HPassword;
            _dbContext.Users.Add(User);
            _dbContext.SaveChanges();

            return true;
        }

        public bool DeleteUser(int id)
        {
            var user = _dbContext.Users.FirstOrDefault(x => x.Id == id);
            if (user == null) return false;
            _dbContext.Users.Remove(user);
            _dbContext.SaveChanges();
            return true;
        }

        public bool EditUser(int id, UserEditForAdminDto userToEdit)
        {
            var user = _dbContext.Users.Include(n=>n.Role).FirstOrDefault(x => x.Id == id);

            if (user == null) return false;

            user.RoleId = userToEdit.RoleId;
            user.IsActivated = userToEdit.IsActivated;
            user.IsBanned = userToEdit.IsBanned;
            user.GroupNumber = userToEdit.GroupNumber;
            user.Phone = userToEdit.Phone;
            user.UserName = userToEdit.UserName;
            user.Email = userToEdit.Email;

            _dbContext.SaveChanges();
            return true;
        }

        public (UserDetailsDto NUser, UserAdminDetailsDto AUser) GetDeatails()
        {
            (UserDetailsDto NUser, UserAdminDetailsDto AUser) userData = (null,null);

            var userId = (int)_userContext.GetUserId;

            var user = _dbContext.Users.Include(n=>n.Role).FirstOrDefault(x => x.Id == userId);
            
            if(user.Role.Name == "admin")
            {
                userData = (null, _mapper.Map<UserAdminDetailsDto>(user));     
            }else if(user.Role.Name == "manager" || user.Role.Name == "worker")
            {
                userData = (_mapper.Map<UserDetailsDto>(user),null);
            }
            else if (user.Role.Name == null)
            {
                return (null,null);
            }

            return userData;
        }

        public bool EditProfile(NUserEditDto userEdit)
        {
            var userId = (int)_userContext.GetUserId;
            var user = _dbContext.Users.Include(n => n.Role).FirstOrDefault(x => x.Id == userId);

            if (user.Role.Name == "admin") return false;

            user.Email = userEdit.Email;
            user.Phone = userEdit.Phone;
            user.UserName = userEdit.UserName;
            _dbContext.SaveChanges();
            return true;
        }

        public void ChangePassword(ChangePasswordDto newPassword)
        {
            var userId = (int)_userContext.GetUserId;
            var user = _dbContext.Users.FirstOrDefault(x => x.Id == userId);

            var HPassword = _passwordHasher.HashPassword(user,newPassword.Password);

            user.PasswordHash = HPassword;
            _dbContext.SaveChanges();

        }

        

    }
}
