﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using visaRestApi.Entities;
using visaRestApi.Models.TaskManagment;


namespace visaRestApi.Services
{
    public interface ITasksService
    {
        void CreateTask(CreateTaskDto newTask, int slaveId);
        List<SlaveUserDto> GetYourUsers();
        List<UsersAllTasksDto> GetAllTasks(int slaveId);
        bool EditTask(ManagerEditTaskDto newNote, int taskId);
        bool AcceptUserTaskEnd(int taskId);
        public bool DeleteTask(int taskId);
        bool EditUserTask(int taskId, string newNotes);
        bool finnalizeTask(int id);
        List<UserTaskDto> getYourTask();
    }

    public class TasksService : ITasksService
    {
        private readonly visaRestApiDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly IUserContextService _userContext;

        public TasksService(visaRestApiDbContext dbContext, IMapper mapper, IUserContextService userContext)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _userContext = userContext;
        }

        public List<SlaveUserDto> GetYourUsers()
        {
            var userId = _userContext.GetUserId;
            var userMaster = _dbContext.Users.FirstOrDefault(x => x.Id == userId);

            var slaveUsers = _dbContext.Users.Where(x => x.GroupNumber == userMaster.GroupNumber && x.Role.Name=="worker").ToList();

            return _mapper.Map<List<SlaveUserDto>>(slaveUsers);

        }
        public void CreateTask(CreateTaskDto newTask, int slaveId)
        {
            var userId = _userContext.GetUserId;
            var userMaster = _dbContext.Users.FirstOrDefault(x => x.Id == userId);
            var userSlave = _dbContext.Users.FirstOrDefault(n => n.Id == slaveId);

            var task = new Tasks()
            {
                Title = newTask.Title,
                UserSlave = userSlave,
                UserMaster = userMaster,
                ManagerNotes = newTask.ManagerNotes,
                CreateDate = DateTime.Now.ToShortDateString(),
                ExpireDate = DateTime.Now.AddDays(30).ToShortDateString(),
     
            };

            _dbContext.UsersTasks.Add(task);
            _dbContext.SaveChanges();

        }
        public List<UsersAllTasksDto> GetAllTasks(int slaveId)
        {

            var taskData = _dbContext.UsersTasks.Where(x => x.UserSlave.Id == slaveId).ToList();
            if (taskData == null) return null;
            return _mapper.Map<List<UsersAllTasksDto>>(taskData);
        }

        public bool EditTask(ManagerEditTaskDto newNote,int taskId)
        {
            var taskData = _dbContext.UsersTasks.FirstOrDefault(x=>x.Id == taskId);
            if (taskData == null)  return false;
            taskData.Title = newNote.Title;
            taskData.Title = newNote.ManagerNotes;
            _dbContext.SaveChanges();

            return true;
        }

        public bool AcceptUserTaskEnd(int taskId)
        {
            var taskData = _dbContext.UsersTasks.FirstOrDefault(x => x.Id == taskId);
            if (taskData == null || taskData.UserFinishTask == false) return false;
            taskData.MenagerAccept = true;
            _dbContext.SaveChanges();
            return true;
            
        }
        public bool DeleteTask(int taskId)
        {
            var taskData = _dbContext.UsersTasks.FirstOrDefault(x => x.Id == taskId);
            if (taskData == null) return false;
            _dbContext.UsersTasks.Remove(taskData);
            _dbContext.SaveChanges();
            return true;
        }

        public bool EditUserTask(int taskId, string newNotes)
        {
            var taskData = _dbContext.UsersTasks.FirstOrDefault(x => x.Id == taskId);
            if (taskData == null) return false;
            taskData.UserNotes = newNotes;
            _dbContext.SaveChanges();
            return true;
        }
        public List<UserTaskDto> getYourTask()
        {
            var userId = _userContext.GetUserId;
            var taskData = _dbContext.UsersTasks.Where(x => x.UserSlave.Id == userId);
            if (taskData==null) return null;

            return _mapper.Map<List<UserTaskDto>>(taskData);
        }
        public bool finnalizeTask(int id)
        {
            var taskData = _dbContext.UsersTasks.FirstOrDefault(x => x.Id == id);
            if (taskData == null || taskData.UserFinishTask == false) return false;
            taskData.MenagerAccept = true;
            _dbContext.SaveChanges();
            return true;
        }

        
    }
}
