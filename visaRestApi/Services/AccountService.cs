﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using visaRestApi.Entities;
using visaRestApi.Models.AccountManagemt;
using visaRestApi.Models.Admin;

namespace visaRestApi.Services
{
    public interface IAccountService
    {
        void RegisterUser(RegsiterUserDto dto);
        string GenerateToken(LoginUserDto dto);
        bool ActiveUser(int userId, ActiveUserOrBanDto dto);
        bool BanUser(int userId, ActiveUserOrBanDto dto);
    }

    public class AccountService : IAccountService
    {
        private readonly visaRestApiDbContext _dbcontext;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly AuthenticationSettings _authenticationSettings;

        public AccountService(visaRestApiDbContext dbcontext, IPasswordHasher<User> passwordHasher, AuthenticationSettings authenticationSettings)
        {
            _dbcontext = dbcontext;
            _passwordHasher = passwordHasher;
            _authenticationSettings = authenticationSettings;
        }

        public void RegisterUser(RegsiterUserDto dto)
        {
            var newUser = new User()
            {
                Email = dto.Email,
                UserName = dto.UserName,
                Phone = dto.Phone,
                RoleId = dto.RoleId,
                GroupNumber = dto.GroupNumber
            };
            var HPassword = _passwordHasher.HashPassword(newUser, dto.Password);
            newUser.PasswordHash = HPassword;
            _dbcontext.Users.Add(newUser);
            _dbcontext.SaveChanges();
        }
        public bool ActiveUser(int userId,ActiveUserOrBanDto dto)
        {

            var user = _dbcontext.Users.FirstOrDefault(n => n.Id == userId);
            if (user == null) return false;

            user.IsActivated = user.IsActivated == false ? true : false;
            _dbcontext.SaveChanges();
            return true;
            
        }
        public bool BanUser(int userId, ActiveUserOrBanDto dto)
        {
            var user = _dbcontext.Users.FirstOrDefault(n => n.Id == userId);
            if (user == null) return false;

            user.IsBanned = user.IsBanned == false ? true : false;
            _dbcontext.SaveChanges();
            return true;

        }

        public string GenerateToken(LoginUserDto dto)
        {
            var user = _dbcontext.Users.Include(n=>n.Role).FirstOrDefault(n => n.Email == dto.Email);
            if(user is null)
            {
                return "WrongUser";
            }

            var result = _passwordHasher.VerifyHashedPassword(user, user.PasswordHash, dto.Password);
            if(result == PasswordVerificationResult.Failed)
            {
                return "WrongPassword";
            }
            if (user.IsActivated == false)
            {
                return "userIsNotActivated";
            }
            if(user.IsBanned == true)
            {
                return "userIsBanned";
            }


            var claims = new List<Claim>()
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(ClaimTypes.Role, user.Role.Name),
                new Claim(ClaimTypes.Email, user.Email),
                new Claim(ClaimTypes.GroupSid, user.GroupNumber.ToString()),
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_authenticationSettings.JwtKey));
            var cred = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddDays(_authenticationSettings.JwtExpireDate);

            var token = new JwtSecurityToken(
                _authenticationSettings.JwtIssuer,
                _authenticationSettings.JwtIssuer,
                claims,
                expires: expires,
                signingCredentials: cred
                );
            var tokenHandler = new JwtSecurityTokenHandler();
            return tokenHandler.WriteToken(token);
        }

        
    }
}
