﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using visaRestApi.Entities;
using visaRestApi.Models;

namespace visaRestApi.Services
{
    public interface IRoleService
    {
        int Create(CreateRoleDto newRole);
        IEnumerable<UserRoleDto> GetAll();
        bool Delete(int roleId);
        bool Edit(int roleId, string newRoleName);
        string GetOne(int id);
    }

    public class RoleService : IRoleService
    {
        private readonly visaRestApiDbContext _dbContext;
        private readonly IMapper _mapper;

        public RoleService(visaRestApiDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public IEnumerable<UserRoleDto> GetAll()
        {
            var roles = _dbContext.UsersRoles.ToList();

            if (roles is null) return null;

            var result = _mapper.Map<IEnumerable<UserRoleDto>>(roles);

            return result;
        }

        public int Create(CreateRoleDto newRole)
        {
            var role = _mapper.Map<Role>(newRole);
            _dbContext.UsersRoles.Add(role);
            _dbContext.SaveChanges();

            return role.Id;
        }

        public bool Edit(int id, string newRoleName)
        {
            var role = _dbContext.UsersRoles.FirstOrDefault(n=>n.Id == id);
            if (role == null) return false;
 
            role.Name = newRoleName;
            _dbContext.SaveChanges();
            return true;
        }

        public string GetOne(int id)
        {
            var role = _dbContext.UsersRoles.FirstOrDefault(n => n.Id == id);
            if (role == null) return null;

            return role.Name;
        }

        public bool Delete(int roleId)
        {
            var role = _dbContext.UsersRoles.FirstOrDefault(n => n.Id == roleId);

            if (role == null) return false;

            _dbContext.UsersRoles.Remove(role);
            _dbContext.SaveChanges();

            return true;
        }
    }
}
