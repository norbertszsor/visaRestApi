﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace visaRestApi.Migrations
{
    public partial class node : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UsersTasks_Users_MasterID",
                table: "UsersTasks");

            migrationBuilder.DropForeignKey(
                name: "FK_UsersTasks_Users_SlaveID",
                table: "UsersTasks");

            migrationBuilder.DropTable(
                name: "UsersNotes");

            migrationBuilder.RenameColumn(
                name: "SlaveID",
                table: "UsersTasks",
                newName: "SlaveUser");

            migrationBuilder.RenameColumn(
                name: "MasterID",
                table: "UsersTasks",
                newName: "MasterUser");

            migrationBuilder.RenameColumn(
                name: "Content",
                table: "UsersTasks",
                newName: "UserNotes");

            migrationBuilder.RenameIndex(
                name: "IX_UsersTasks_SlaveID",
                table: "UsersTasks",
                newName: "IX_UsersTasks_SlaveUser");

            migrationBuilder.RenameIndex(
                name: "IX_UsersTasks_MasterID",
                table: "UsersTasks",
                newName: "IX_UsersTasks_MasterUser");

            migrationBuilder.AddColumn<string>(
                name: "FinishData",
                table: "UsersTasks",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "ManaerAccept",
                table: "UsersTasks",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "ManagerNotes",
                table: "UsersTasks",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "UserFinishTask",
                table: "UsersTasks",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddForeignKey(
                name: "FK_UsersTasks_Users_MasterUser",
                table: "UsersTasks",
                column: "MasterUser",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UsersTasks_Users_SlaveUser",
                table: "UsersTasks",
                column: "SlaveUser",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UsersTasks_Users_MasterUser",
                table: "UsersTasks");

            migrationBuilder.DropForeignKey(
                name: "FK_UsersTasks_Users_SlaveUser",
                table: "UsersTasks");

            migrationBuilder.DropColumn(
                name: "FinishData",
                table: "UsersTasks");

            migrationBuilder.DropColumn(
                name: "ManaerAccept",
                table: "UsersTasks");

            migrationBuilder.DropColumn(
                name: "ManagerNotes",
                table: "UsersTasks");

            migrationBuilder.DropColumn(
                name: "UserFinishTask",
                table: "UsersTasks");

            migrationBuilder.RenameColumn(
                name: "UserNotes",
                table: "UsersTasks",
                newName: "Content");

            migrationBuilder.RenameColumn(
                name: "SlaveUser",
                table: "UsersTasks",
                newName: "SlaveID");

            migrationBuilder.RenameColumn(
                name: "MasterUser",
                table: "UsersTasks",
                newName: "MasterID");

            migrationBuilder.RenameIndex(
                name: "IX_UsersTasks_SlaveUser",
                table: "UsersTasks",
                newName: "IX_UsersTasks_SlaveID");

            migrationBuilder.RenameIndex(
                name: "IX_UsersTasks_MasterUser",
                table: "UsersTasks",
                newName: "IX_UsersTasks_MasterID");

            migrationBuilder.CreateTable(
                name: "UsersNotes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Content = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreateDate = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ExpireDate = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UsersNotes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UsersNotes_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UsersNotes_UserId",
                table: "UsersNotes",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_UsersTasks_Users_MasterID",
                table: "UsersTasks",
                column: "MasterID",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UsersTasks_Users_SlaveID",
                table: "UsersTasks",
                column: "SlaveID",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
