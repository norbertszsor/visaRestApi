﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace visaRestApi.Migrations
{
    public partial class dasdwaa : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "RoleName",
                table: "UsersRoles",
                newName: "Name");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Name",
                table: "UsersRoles",
                newName: "RoleName");
        }
    }
}
