﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace visaRestApi.Migrations
{
    public partial class dddd4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ManaerAccept",
                table: "UsersTasks",
                newName: "MenagerAccept");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "MenagerAccept",
                table: "UsersTasks",
                newName: "ManaerAccept");
        }
    }
}
