﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace visaRestApi.Migrations
{
    public partial class addusergroup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserMasterId",
                table: "UsersTasks");

            migrationBuilder.DropColumn(
                name: "UserSlaveId",
                table: "UsersTasks");

            migrationBuilder.AddColumn<int>(
                name: "GroupNumber",
                table: "Users",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GroupNumber",
                table: "Users");

            migrationBuilder.AddColumn<int>(
                name: "UserMasterId",
                table: "UsersTasks",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserSlaveId",
                table: "UsersTasks",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
